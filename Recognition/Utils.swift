//
//  Utils.swift
//  Speech
//
//  Created by Ivan Kh on 15/08/2017.
//  Copyright © 2017 Google. All rights reserved.
//

import JavaScriptCore

extension JSValue {
    
    func invokeMethodAsync(_ method: String!, _ arguments: [Any]? = nil) {
        let args = NSMutableDictionary()
        args[method] = arguments ?? []
        perform(#selector(_invokeMethod(_:)), with: args, afterDelay: 0.0)
    }

    @objc private func _invokeMethod(_ args: NSDictionary) {
        invokeMethod(args.allKeys[0] as! String, withArguments: args.allValues[0] as! [Any])
    }
}

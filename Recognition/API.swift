//
//  API.swift
//  Speech
//
//  Created by Ivan Kh on 15/08/2017.
//  Copyright © 2017 Google. All rights reserved.
//

import AVFoundation
import JavaScriptCore
import googleapis

fileprivate let JSArrayPush = "push"
fileprivate let webkitSpeechRecognitionResultPropertyIsFinal = "isFinal"
fileprivate let webkitSpeechRecognitionOnStart = "onstart"
fileprivate let webkitSpeechRecognitionOnEnd = "onend"
fileprivate let webkitSpeechRecognitionOnResult = "onresult"

fileprivate let SAMPLE_TIMEOUT = 0.1    // seconds/chunk
fileprivate let BYTES_PER_SAMPLE = 2.0  // bytes/sample

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ResultEvent
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@objc protocol webkitSpeechRecognitionResultEventProtocol: JSExport {
    var resultIndex: Int { get }
    var results: [JSValue] { get }
}

@objc class webkitSpeechRecognitionResultEvent: NSObject, webkitSpeechRecognitionResultEventProtocol {
    var resultIndex: Int = 0
    var results = [JSValue]()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Alternative
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@objc protocol webkitSpeechRecognitionAlternativeProtocol: JSExport {
    var transcript: String { get }
}

@objc class webkitSpeechRecognitionAlternative : NSObject, webkitSpeechRecognitionAlternativeProtocol {
    var transcript: String
    
    init(_ transcript: String) {
        self.transcript = transcript
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Recognition protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@objc protocol webkitSpeechRecognitionProtocol: JSExport {

    init()
    
    var lang: String { get set }
    var apiKey: String { get set }

    func start()
    func stop()
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Recognition implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@objc class webkitSpeechRecognition : NSObject, webkitSpeechRecognitionProtocol, AudioControllerDelegate {
    
    var lang = String()
    var apiKey = String()

    private var audioData: NSMutableData!
    private var selfJS: JSValue?
    private var contextJS: JSContext?
    
    override required init() {
    }
    
    func start() {
        API_KEY = self.apiKey
        
        contextJS = JSContext.current()
        selfJS = JSContext.currentThis()
        selfJS!.invokeMethodAsync(webkitSpeechRecognitionOnStart)
        
        // exec in background to prevent UI lag
        
        DispatchQueue.global().async {
            let audioSession = AVAudioSession.sharedInstance()
            try! audioSession.setCategory(AVAudioSessionCategoryRecord)
            
            audioSession.requestRecordPermission { _ in }
            
            _ = AudioController.sharedInstance.prepare(specifiedSampleRate: SAMPLE_RATE)
            
            self.audioData = NSMutableData()
            AudioController.sharedInstance.delegate = self
            _ = AudioController.sharedInstance.start()
            SpeechRecognitionService.sharedInstance.lang = self.lang
        }
    }
    
    func stop() {
        _ = AudioController.sharedInstance.stop()
        SpeechRecognitionService.sharedInstance.stopStreaming()
        selfJS!.invokeMethodAsync(webkitSpeechRecognitionOnEnd)
    }
    
    func processSampleData(_ data: Data) -> Void {
        audioData.append(data)
        
        // We recommend sending samples in 100ms chunks (bytes/chunk)
        let chunkSize = Int(SAMPLE_TIMEOUT
            * Double(SAMPLE_RATE)
            * BYTES_PER_SAMPLE)
        
        if (audioData.length > chunkSize) {
            SpeechRecognitionService.sharedInstance.streamAudioData(audioData,
                                                                    completion:
                { [weak self](response, error) in
                    let eventArg = webkitSpeechRecognitionResultEvent()
                    
                    if let error = error {
                        print(String(describing: error))
                    }
                    else if let response = response {
                        var index = 0
                        
                        // convert Google API response to Webkit strutures
                        
                        for result in response.resultsArray! {
                            if let result = result as? StreamingRecognitionResult {
                                
                                let resultJS = JSValue(newArrayIn: self?.contextJS!)!
                                
                                resultJS.setValue(result.isFinal,
                                                  forProperty: webkitSpeechRecognitionResultPropertyIsFinal)
                                
                                for i in result.alternativesArray {
                                    resultJS.invokeMethod(JSArrayPush, withArguments:
                                        [webkitSpeechRecognitionAlternative(
                                            (i as! SpeechRecognitionAlternative).transcript)])
                                }
                                
                                eventArg.results.append(resultJS)
                                
                                if result.isFinal {
                                    eventArg.resultIndex = index
                                }
                                
                                index += 1
                            }
                        }
                        
                        self?.selfJS!.invokeMethodAsync(webkitSpeechRecognitionOnResult, [eventArg])
                    }
            })
            self.audioData = NSMutableData()
        }
    }
}
